import shutil
import argparse
import os
import random
import sys
from pathlib import Path


def main(opt):
    files=os.listdir(opt.labelSource)
    for file in files:
        print(os.path.join(opt.labelSource, file))
        name = os.path.splitext(file)[0]+'.jpg'
        labelpath = os.path.join(opt.labelSource, file)  # 文件存放路径
        imagepath = os.path.join(opt.imageSource, name)  # 图片
        ran=random.randint(0,100)
        if ran>opt.rate:
            pathL = opt.valLabel  #  更改路径为测试集
            pathI = opt.valImage
        else:
            pathL = opt.trainLabel  #  更改路径为训练集
            pathI = opt.trainImage
        movelabelpath = os.path.join(pathL, file)  # movepath：指定移动文件夹
        moveimagepath = os.path.join(pathI, name)
        shutil.move(labelpath, movelabelpath)
        shutil.move(imagepath, moveimagepath)

def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--labelSource', type=str, default='D:/data/yolov5-recognition/dataset/labels/yolo')
    parser.add_argument('--valLabel', type=str, default='D:/data/yolov5-recognition/dataset/labels/va')
    parser.add_argument('--trainLabel', type=str, default='D:/data/yolov5-recognition/dataset/labels/tr')
    parser.add_argument('--rate', type=int, default=80 )
    parser.add_argument('--imageSource', type=str, default='D:/data/yolov5-recognition/dataset/images/yolo')
    parser.add_argument('--valImage', type=str, default='D:/data/yolov5-recognition/dataset/images/va')
    parser.add_argument('--trainImage', type=str, default='D:/data/yolov5-recognition/dataset/images/tr')
    opt = parser.parse_args()
    return opt

if __name__ == "__main__":
    opt = parse_opt()
    main(opt)